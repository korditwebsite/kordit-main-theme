<?php 
// check if the flexible content field has rows of data
if( have_rows('pojedyncza_sekcja') ):

     // loop through the rows of data
	while ( have_rows('pojedyncza_sekcja') ) : the_row();


	/////////////////////////
	/////   HOME   /////////
	///////////////////////

		//slider
		if( get_row_layout() == 'slider' ): 
			get_template_part( 'templates/hero/slider/init'); 

		//Choose one
		elseif( get_row_layout() == 'choose_one' ):
			add_css_theme("slider-1", "/templates/hero/choose/scss/style.css");
			get_template_part( 'templates/hero/choose'); 

		//Present logotype
		elseif( get_row_layout() == 'present_logotype' ) :
			get_template_part( 'templates/hero/inital');   

	/////////////////////////
	/////   ABOUT US   /////
	///////////////////////

		//grafika z opisem
		elseif( get_row_layout() == 'grafika_z_opisem' ):
			get_template_part( 'templates/textimage/init');

	/////////////////////////
	/////    ADDONS    ////
	///////////////////////

		//logo carousel
		elseif( get_row_layout() == 'logo_carousel' ):
			add_css_theme("carousel-logo", "/templates/addons/logo-carousel/scss/style.css");
			get_template_part( 'templates/addons/logo-carousel/logo-1'); 

		//team
		elseif( get_row_layout() == 'team' ):
			add_css_theme("team-css", "/templates/addons/employe/scss/style.css");
			get_template_part( 'templates/addons/employe/employe');      

		//galeria
		elseif( get_row_layout() == 'galeria' ):
			get_template_part( 'templates/addons/gallery/gallery');  

		//paralax
		elseif( get_row_layout() == 'paralax' ):
			add_css_theme("paralax-css", "/templates/addons/paralax/scss/style.css");
			get_template_part( 'templates/addons/paralax/paralax');     

		//collapse   
		elseif( get_row_layout() == 'collapse' ):
			get_template_part( 'templates/collapse/init'); 

		elseif( get_row_layout() == 'list' ):
			add_css_theme("paralax-css", "/templates/addons/list/scss/style.css");
			get_template_part( 'templates/addons/list/list');
		elseif( get_row_layout() == 'textbox' ):
			get_template_part( 'templates/addons/content/content');  
			

	/////////////////////////
	/////      CPT     /////
	///////////////////////

		//cpt
		elseif( get_row_layout() == 'show_cpt' ):
			get_template_part( 'templates/cpt/cpt');  


	/////////////////////////
	/////    SERVICE   /////
	///////////////////////

		elseif( get_row_layout() == 'offer' ):
			get_template_part( 'templates/offer/init');      


	/////////////////////////
	/////    CONTACT   /////
	///////////////////////
		elseif( get_row_layout() == 'kontakt' ):
			get_template_part( 'templates/contact/init');    

		endif;

	endwhile;

else :
	echo '<section class="container-guttenberg">';
	the_content();
	echo '</section>';
endif;
