<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="keywords" content="<?php the_field("slowa_kluczowe", "options"); ?>" />
	<?php wp_head(); ?>
	<?php if (is_page_template( 'single.php' )): ?>
		<title><?php //wp_title(); ?></title>
	<?php endif ?>
	
</head>
<body <?php body_class( 'class-name' ); ?>>
	<?php
	if ( have_rows( 'header-settings', 'option' ) ) : 
		while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
			$wyborheadera = get_sub_field( 'header_sekcja' ); 
		endwhile;
	endif;
	?>
	<header id="header-<?php echo $wyborheadera; ?>">
		<?php get_template_part( 'templates/header/init');?>
	</header>
	<?php if ( have_rows( 'ustawnienia_footera', 'option' ) ) : 
		while ( have_rows( 'ustawnienia_footera', 'option' ) ) : the_row();
			$tlo = wp_get_attachment_image_url( get_sub_field('background_header'), "hero_image" );
			$opacity = get_sub_field( 'opacity' );
			$bgcolor = get_sub_field( 'color_background' );
			$fontcolor = get_sub_field( 'color_h1' );
			$fonttransform = get_sub_field( 'uppercase' );

		endwhile; endif; ?>
		<?php if ( !is_page_template( 'homepage.php' ) ): ?>
			<div class="header-bg" style="background-color: <?php echo $bgcolor; ?>;">
				<div class="bg-image" style="background-image: url(<?php echo $tlo; ?>);opacity: <?php echo $opacity; ?>;"></div>
				<div class="container">
					<div class="header-present">
						<h1 style="color: <?php echo $fontcolor; ?>; text-transform: <?php echo $fonttransform; ?> "><?php the_title(); ?></h1>
					</div>
				</div>
			</div>
			<?php endif ?>