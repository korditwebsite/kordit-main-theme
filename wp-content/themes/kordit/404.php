<?php get_header(); ?>
<main id="404">
	<div class="container">
		<div class="row direct-column">
			<h1 class="text-center">Przepraszamy, strona nie została odnaleziona</h1>
			<h2>404</h2>
			<a href="/"><button>Wróć do strony głównej</button></a>
			<div class="test" style="min-height: 400px;">
				<?php 
				
				?>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>