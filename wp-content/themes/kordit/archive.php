<?php get_header(); ?>
<main id="blog-page">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<section id="blog">
					<div class="container">
						<div class="row">
							<?php 
							if ( have_posts() ) {
								while ( have_posts() ) {
									the_post(); 
									$parent_id = $post->post_parent;
									?>
									<div class="col-xl-4">
										<div class="inner-post-list">
											<div class="thumbnail">
												<?php the_post_thumbnail( 'category-thumb' ); ?>
											</div>
											<div class="inner-content">
												<div class="title">
													<?php the_title(); ?>
												</div>
												<div class="excerpt">
													<?php 
													if (has_excerpt()) {
														the_excerpt();
													} else {
														echo wp_trim_words( get_the_excerpt(), 20 );
													}

													?>
												</div>
												<div class="link">
													<a href="<?php echo get_permalink($parent_id); ?>">
														<?php _e( 'read more', 'textdomain' ); ?>
													</a>
												</div>
											</div>
										</div>
									</div>
								<?php } } ?>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</main>
	<?php get_footer(); ?>