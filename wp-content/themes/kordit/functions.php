<?php

//Dodawanie widgetów
include_once( get_stylesheet_directory() .'/assets/includes/widget.php');


//Dodawanie rozmiarów miniaturek
include_once( get_stylesheet_directory() .'/assets/includes/size-thumbnail.php');

//Inicjacja plików motywu
function theme_enqueue_scripts() {
    wp_enqueue_style( 'normalize', get_template_directory_uri() . '/assets/css/normalize.css' );
    wp_enqueue_style( 'Bootstrap_css', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/css/animate.css' );
    wp_enqueue_style( 'Style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/sass/main-theme.css' );

    wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/assets/js/jquery.js', array(), '3.3.1', true );
    wp_enqueue_script( 'bundle',  get_template_directory_uri() . '/assets/js/bootstrap.bundle.min.js' );
    wp_enqueue_script( 'Bootstrap',  get_template_directory_uri() . '/assets/js/bootstrap.min.js' );
    wp_enqueue_script( 'apear', get_template_directory_uri() . '/assets/js/apear.js', array());
    wp_enqueue_script( 'Main', get_template_directory_uri() . '/assets/js/initial.js', array(), '1.0.0', true );
    wp_enqueue_script( 'my-js', get_template_directory_uri() . '/assets/js/myjs.js', array(), '1.0.0', true );
    include_once( get_stylesheet_directory() .'/assets/includes/setup.php');
}
//Dodawanie bibliotek z pozycji panelu admina


add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );


//Dodawanie wsparcia dla motywu
include_once( get_stylesheet_directory() .'/assets/includes/support.php');

//Dodawanie wsparcia dla CPT
include_once( get_stylesheet_directory() .'/assets/includes/cpt.php');


//Tworzenie automatycznej struktury nawigacji
include_once( get_stylesheet_directory() .'/assets/includes/walker-nav-menu.php');


//dodawanie CPT
include_once( get_stylesheet_directory() .'/assets/includes/cpt.php');

// Register WordPress nav menu
register_nav_menu('top', 'Top menu');
register_nav_menu('footer', 'Footer menu');
register_nav_menu('menu', 'Karta menu');

//Podmiana acf z panel admin na plik
//require_once( get_template_directory() . '/acf.php' );