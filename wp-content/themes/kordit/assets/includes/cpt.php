<?php
if (get_field( 'active_portfolio', 'option' )) {
    include_once( get_stylesheet_directory() .'/assets/includes/CPT/portfolio.php');
}

if (get_field( 'active_multigallery', 'option' )) {
    include_once( get_stylesheet_directory() .'/assets/includes/CPT/multigallery.php');
}

if (get_field( 'active_service', 'option' )) {
    include_once( get_stylesheet_directory() .'/assets/includes/CPT/service.php');
}
