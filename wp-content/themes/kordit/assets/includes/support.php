<?php

//Wsparcie dla breadcrumbs
add_theme_support( 'yoast-seo-breadcrumbs' );

//Dodanie miniaturek do strony
add_theme_support( 'post-thumbnails' ); 

//Dodawanie strony z opcjami
// if( function_exists('acf_add_options_page') ) {
// 	acf_add_options_page();
// }
if( function_exists('acf_add_options_page') ) {

 	// add parent
	$parent = acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title' 	=> 'Theme Settings',
		'redirect' 		=> false
	));


	// add sub page
	acf_add_options_sub_page(array(
		'page_title' 	=> 'CPT Settings',
		'menu_title' 	=> 'CPT',
		'parent_slug' 	=> $parent['menu_slug'],
	));

}

//Logo img
if (!function_exists('quality_construction_custom_logo_setup')) :
	function quality_construction_custom_logo_setup()
	{
		add_theme_support('custom-logo', array(
			'height' => 240,
			'width' => 400,
			'flex-width' => true,
		));
	}
	add_action('after_setup_theme', 'quality_construction_custom_logo_setup');
endif;

//Wsparcie dla wyświetlania title
add_theme_support( 'title-tag' );


//Export content SVG 
// Funkcji wystarczy podać link w motywie do svg. Działa na zasadzie "exportsvg("assets/img/adress.svg");". 
function exportsvg($linksvg) {
	$link = get_template_directory_uri() . $linksvg;
	$svg_file = file_get_contents($link);
	$find_string = "<svg";
	$position = strpos($svg_file, $find_string);
	$svg_file_new = substr($svg_file, $position);
	echo $svg_file_new;
}
function exportsvg_url($linksvg) {
	$link = $linksvg;
	$svg_file = file_get_contents($link);
	$find_string = "<svg";
	$position = strpos($svg_file, $find_string);
	$svg_file_new = substr($svg_file, $position);
	echo $svg_file_new;
}

//Background id

function id_subimage($acffield, $size) {
	return wp_get_attachment_image( get_sub_field($acffield), $size );
} 
function id_subimage_url($acffield, $size) {
	return wp_get_attachment_image_url( get_sub_field($acffield), $size );
} 
function id_image($acffield, $size) {
	return wp_get_attachment_image( get_field($acffield), $size );
} 
function id_image_url($acffield, $size) {
	return wp_get_attachment_image_url( get_field($acffield), $size );
} 

//Dynamiczne dodawanie cssa
function add_css_theme($name, $linkcss) {
	wp_enqueue_style( $name, get_template_directory_uri() . $linkcss,true,'1.0','all');
}
function add_js_theme($name, $linkjs) {
	wp_enqueue_script( $name, get_template_directory_uri() . $linkjs, array(), '1.0.0', true );
}

//Możliwość dodawania SVG
function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

//Wsparcie dla admin bar
function admin_bar(){
	if(is_user_logged_in()){
		add_filter( 'show_admin_bar', '__return_true' , 1000 );
	}
}
add_action('init', 'admin_bar' );

//Skrócony tytuł
function short_filter_wp_title( $title ) {
	if ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {
		$title = single_post_title( '', false );
	}
	if ( is_front_page() && ! is_page() ) {
		$title = esc_attr( get_bloginfo( 'name' ) );
	}
	return $title;
	add_filter( 'wp_title', 'short_filter_wp_title', 100);
}
add_post_type_support( 'page', 'excerpt' );


function color_inverse($color){
	$color = str_replace('#', '', $color);
	if (strlen($color) != 6){ return '000000'; }
	$rgb = '';
	for ($x=0;$x<3;$x++){
		$c = 255 - hexdec(substr($color,(2*$x),2));
		$c = ($c < 0) ? 0 : dechex($c);
		$rgb .= (strlen($c) < 2) ? '0'.$c : $c;
	}
	return '#'.$rgb;
}
