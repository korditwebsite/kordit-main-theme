<?php if ( have_rows( 'ustawnienia_footera', 'option' ) ) : 
	while ( have_rows( 'ustawnienia_footera', 'option' ) ) : the_row();
		$linkfb = get_sub_field( 'link_do_facebooka' );
		$widgetfb = get_sub_field( 'wyswietl_widget_z_boku' ); 
		$wyborfootera = get_sub_field( 'wyglad_footera' ); 
	endwhile; endif; ?>
	<footer id="footer-<?php echo $wyborfootera; ?>">
		<?php get_template_part( 'templates/footer/init');?>
	</footer>

	<?php wp_footer(); ?>
	<?php if ($widgetfb) : ?>
		<div class="fb-page" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
		<div class="face-slider" id="face-slider">
			<div id="tab"><img src="/wp-content/themes/kordit/assets/img/facebook.jpg"></div>
			<div id="face-code"><div class="fb-page" data-href="<?php echo $linkfb; ?>" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo $linkfb; ?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $linkfb; ?>"></a></blockquote></div></div>
		</div>
		<script>(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v3.1&appId=2001495613512113&autoLogAppEvents=1';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
<?php endif ?>
</body>
</html>
