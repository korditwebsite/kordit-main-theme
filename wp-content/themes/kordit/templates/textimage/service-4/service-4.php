<section class="service-4" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-md-12 col-12 wow fadeInLeft">
				<div class="content">
					<h2><?php the_sub_field("tytul"); ?></h2>
					<p><?php the_sub_field("tresc"); ?></p>
					<a href="/o-firmie/">Czytaj więcej</a>
				</div>
			</div>
			<div class="col-xl-6 col-md-12 col-12 wow fadeInRight">
				<video class="" controls>
					<source src="/wp-content/themes/kordit/video/alurama.mp4" type="video/mp4" />
				</video>
			</div>
		</div>
	</div>
</section>