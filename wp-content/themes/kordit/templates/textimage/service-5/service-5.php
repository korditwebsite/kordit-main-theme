<section class="service-5" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-6 col-md-12 col-12 wow fadeInLeft services-5-img-box">
				<div class="thumbnail responsive">
					<?php $grafika = wp_get_attachment_image_url( get_sub_field('grafika'), "kontener" ); ?>
					<?php echo wp_get_attachment_image( get_sub_field('grafika'), "kontener", "", array( "class" => "lazy about-img img-fluid", "data-src=" => $grafika ) );  ?>
				</div>
			</div>
			<div class="col-xl-6 col-md-12 col-12 wow fadeInRight service-5-box">
				<div class="content">
					<h2><?php the_sub_field("tytul"); ?></h2>
					<p><?php the_sub_field("tresc"); ?></p>
					<div class="corner-right"></div>
					<button class="modalButton btn btn-outline-light" data-popup="popupOne">Zobacz rzut</button>
					<div class="modal modalWindow" id="popupOne">
						<div class="modalWrapper">
							<img src="/wp-content/themes/kordit/img/rzut.jpg" alt="rzut domu" class="img-fluid">
							
						</div>
						<a class="closeBtn">CLOSE X</a>
					</div>
					<div class="modal overlay"></div>
				</div>
			</div>
		</div>
	</div>
</section>