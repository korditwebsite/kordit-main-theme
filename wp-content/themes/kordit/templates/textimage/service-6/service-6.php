<section class="about-1" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6">
				<div class="thumbnail responsive about-1-img">
					<?php  echo wp_get_attachment_image( get_sub_field('grafika'), "kontener" ); ?>
				</div>
			</div>
			<div class="col-lg-6">
				<?php if( have_rows('about-simple') ):  while ( have_rows('about-simple') ) : the_row();  ?>
					<div class="about-box">
						<div class="check-box">
							<img src="/wp-content/themes/kordit/img/check.png" alt="check" class="check">
						</div>
						<div class="desc-box">
							<h3><?php the_sub_field("tytul"); ?></h3>
							<p><?php the_sub_field("tresc"); ?></p>
						</div>
					</div>
					
				<?php endwhile; else : endif; ?>
			</div>
		</div>
	</div>
</section>