<?php
$numersekcji = get_sub_field("ulozenie");

if (get_sub_field("ulozenie") == 1) {
	add_css_theme("multiple-1", "/templates/collapse/multiple/scss/style.css");
	get_template_part( '/templates/collapse/multiple/multiple'); 
}
elseif (get_sub_field("ulozenie") == 2) {
	add_css_theme("acordion-1", "/templates/collapse/accordion/scss/style.css");
	get_template_part( '/templates/collapse/accordion/accordion'); 
}