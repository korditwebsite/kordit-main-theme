<?php 
//size container
if ( have_rows( 'size_container' ) ) : while ( have_rows( 'size_container' ) ) : the_row(); 
	$mobile = get_sub_field( 'mobile' );
	$tablet = get_sub_field( 'tablet' );
	$desktop = get_sub_field( 'desktop' );
	$size_container = "col-xl-" . $desktop . " " . "col-md-" . $tablet . " " . "col-" . $mobile;
endwhile; endif; ?>
<?php 
global $post;
$post_slug = $post->post_name;
//Style
if ( have_rows( 'color_container' ) ) : while ( have_rows( 'color_container' ) ) : the_row(); 
	$text_color = get_sub_field( 'text_color' );
	$background_color = get_sub_field( 'background_color' );
	$opacity = get_sub_field( 'opacity' );
	$background_image = id_subimage_url("background_image", "full");
	$invercecolor = color_inverse($text_color);
endwhile; endif; ?>
<section id="accordion" class="<?php the_sub_field( 'id_sekcji' ); ?> multiple" style="background-color: <?php echo $background_color; ?>">
	<div class="background-image" style="opacity: <?php echo $opacity; ?>;" current-url="<?php echo basename(get_permalink()); ?>">
		<div class="bg-image" style="background-image: url(<?php echo $background_image; ?>);"></div>
	</div>
	<?php if ( have_rows( 'accordion' ) ) :  while ( have_rows( 'accordion' ) ) : the_row(); ?>
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="title-container">
						<h3 style="color: <?php echo $text_color; ?>"><?php the_sub_field("title"); ?></h3>
						<p style="color: <?php echo $text_color; ?>"><?php the_sub_field("subtitle"); ?></p>
					</div>
				</div>
				<?php $time = 0; if ( have_rows( 'Questions' ) ) : ?>
				<div class="<?php echo $size_container; ?>">
					<div class="inner-multiple wow fadeInDown" data-wow-delay="<?php echo $time; ?>ms" style="border-color: <?php echo $text_color; ?>;">
						<?php $answer = 1; while ( have_rows( 'Questions' ) ) : the_row(); ?>
						<div class="card">
							<div class="card-header" id="accordion-<?php echo $answer; ?>">
								<h5 class="mb-0" data-toggle="collapse" data-target="#accordion-<?php echo $answer; ?>" aria-expanded="false" aria-controls="collapseTwo">
									<?php the_sub_field("question"); ?>
								</h5>
							</div>
							<div id="accordion-<?php echo $answer; ?>" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
								<div class="card-body">
									<?php the_sub_field("answer"); ?>
								</div>
							</div>
						</div>
						<?php $time = $time + 125; $answer = $answer + 1; endwhile; endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endwhile; endif; ?>
</section>
