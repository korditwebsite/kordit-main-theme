<?php 
$custom_logo_id = get_theme_mod( 'custom_logo' );
$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
?>

<nav class="slide-menu">
	<div class="container-w-logo">

		<div class="w-logotype">
			<?php if ( has_custom_logo() ) {
				echo '<img alt="logotyp" class="img-fluid logotype mono-white" src="'. esc_url( $logo[0] ) .'">';
			} else {
				echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
			} ?>
			
		</div>
	</div>
	<?php
	wp_nav_menu([
		'menu'            => 'top',
		'theme_location'  => 'top',
		'container'       => 'div',
		'container_class' => 'blur-menu__nawigacja-menu',
		'menu_class'      => 'navbar-nav',
		'depth'           => 2,
		'fallback_cb'     => 'bs4navwalker::fallback',
		'walker'          => new bs4navwalker(),
	]);

	if ( have_rows( 'header-settings', 'option' ) ) : 
		while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
			$nbrbtn = get_sub_field( 'button_navi' );
			$wyborheadera = get_sub_field( 'header_sekcja' ); 
		endwhile;
	endif;
	?>
</nav>
<div class="header-blur-container">
	<div class="container-navi">
		<div class="row">
			<div class="col-xl-6">
				
			</div>
			<div class="col-xl-6">
				<div class="open-navigation">
					<div class="hamburger" id="hamburger-<?php echo $nbrbtn; ?>">
						<span class="line"></span>
						<span class="line"></span>
						<span class="line"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>