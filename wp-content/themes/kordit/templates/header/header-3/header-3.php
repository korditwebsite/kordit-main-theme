  <div class="container">
    <div class="row">
      <div class="col-xl-3 col-md-4 col-6">
        <a class="navbar-brand" href="/">
          <div class="brand-logo">
            <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
            $logo = wp_get_attachment_image_src( $custom_logo_id , 'small-logo' );
            if ( has_custom_logo() ) {
              echo '<img class="img-fluid" src="'. esc_url( $logo[0] ) .'">';
            } else {
              echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
            }
            if ( have_rows( 'header-settings', 'option' ) ) : 
              while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
                $nbrbtn = get_sub_field( 'button_navi' );
                $wyborheadera = get_sub_field( 'header_sekcja' ); 
              endwhile;
            endif;
            ?>
          </div>
        </a>
      </div>
      <div class="col-xl-6 col-md-8 offset-xl-3 col-6">
        <?php
        wp_nav_menu([
          'menu'            => 'top',
          'theme_location'  => 'top',
          'container'       => 'div',
          'container_class' => 'mobile-none navi-container-3',
          'menu_id'         => false,
          'menu_class'      => 'navbar-nav',
          'depth'           => 3,
          'fallback_cb'     => 'bs4navwalker::fallback',
          'walker'          => new bs4navwalker()
        ]);
        ?>
        <div class="only-mobile">
          <div class="open-navigation">
            <div class="hamburger" id="hamburger-<?php echo $nbrbtn; ?>">
              <span class="line"></span>
              <span class="line"></span>
              <span class="line"></span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <nav class="mobile-menu slide-menu">
   <?php
   wp_nav_menu([
    'menu'            => 'top',
    'theme_location'  => 'top',
    'container'       => 'div',
    'container_class' => '',
    'menu_id'         => false,
    'menu_class'      => 'navbar-nav',
    'depth'           => 2,
    'fallback_cb'     => 'bs4navwalker::fallback',
    'walker'          => new bs4navwalker()
  ]);
  ?>
</nav>