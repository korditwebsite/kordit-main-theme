<?php
if ( have_rows( 'header-settings', 'option' ) ) : 
	while ( have_rows( 'header-settings', 'option' ) ) : the_row(); 
		$nbrBtn = get_sub_field( 'button_navi' );
		$wyborheadera = get_sub_field( 'header_sekcja' ); 
	endwhile;
endif;
if ($wyborheadera == 1) {
	add_css_theme("header-1", "/templates/header/header-1/scss/style.css");
	get_template_part( 'templates/header/header-1/header', "1" );
}
elseif ($wyborheadera == 2) {
	add_css_theme("header-2", "/templates/header/header-2/scss/style.css");
	get_template_part( 'templates/header/header-2/header', "2" );
}
elseif ($wyborheadera == 3) {
	add_css_theme("header-3", "/templates/header/header-3/scss/style.css");
	get_template_part( 'templates/header/header-3/header', "3" );
}
else {
	echo 'Nie wybrałeś odpowiedniego headera! Przejdź do <a href="/wp-admin/admin.php?page=acf-options-theme-settings"></a> i wybierz odpowiedni!';
}

get_template_part( '/templates/header/hamburger-menu/init'); 
