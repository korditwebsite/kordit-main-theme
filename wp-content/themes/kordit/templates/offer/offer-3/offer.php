<section class="offerbox">
	<div class="container-fluid">
		<div class="row">
			<?php if( have_rows('offer_box') ):  while ( have_rows('offer_box') ) : the_row();  ?>
				<div class="col-lg-3 simple-offer-box">
				<?php echo wp_get_attachment_image( get_sub_field('grafika'), "malyprostokat", "", array( "class" => "lazy, img-fluid", "data-src=" => $grafika ) );  ?>
				<h3><?php the_sub_field("tytul"); ?></h3>
				</div>
			<?php endwhile; else : endif; ?>
		</div>
	</div>
</section>