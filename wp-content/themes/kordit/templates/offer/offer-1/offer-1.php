<?php 
//size container
if ( have_rows( 'size_container' ) ) : while ( have_rows( 'size_container' ) ) : the_row(); 
	$mobile = get_sub_field( 'mobile' );
	$tablet = get_sub_field( 'tablet' );
	$desktop = get_sub_field( 'desktop' );
	$size_container = "col-xl-" . $desktop . " " . "col-md-" . $tablet . " " . "col-" . $mobile;
endwhile; endif; ?>

<?php 
//Style
if ( have_rows( 'color_container' ) ) : while ( have_rows( 'color_container' ) ) : the_row(); 
	$text_color = get_sub_field( 'text_color' );
	$background_color = get_sub_field( 'background_color' );
	$opacity = get_sub_field( 'opacity' );
	$background_image = id_subimage_url("background_image", "full");
	$invercecolor = color_inverse($text_color);
endwhile; endif; ?>
<style type="text/css">
	.service .inner-service:hover {
		background-color: <?php echo $invercecolor; ?>80;
		border-color: <?php echo $invercecolor; ?> !important;
	}
</style>
<?php if ( have_rows( 'service_1' ) ) : while ( have_rows( 'service_1' ) ) : the_row(); ?>
	<section class="<?php the_sub_field( 'id_sekcji' ); ?> service" style="background-color: <?php echo $background_color; ?>">
		<div class="background-image" style="opacity: <?php echo $opacity; ?>;">
			<div class="bg-image" style="background-image: url(<?php echo $background_image; ?>);"></div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<div class="title-container">
						<h3 style="color: <?php echo $text_color; ?>"><?php the_sub_field("title"); ?></h3>
						<p style="color: <?php echo $text_color; ?>"><?php the_sub_field("subtitle"); ?></p>
					</div>
				</div>
				<?php $time = 0; if ( have_rows( 'service' ) ) :  while ( have_rows( 'service' ) ) : the_row(); ?>
				<div class="<?php echo $size_container; ?>">
					<div class="inner-service wow fadeInDown" data-wow-delay="<?php echo $time; ?>ms" style="border-color: <?php echo $text_color; ?>;">
						<?php echo id_subimage("image", "malyprostokat");  ?>
						<h3 style="color: <?php echo $text_color; ?>"><?php the_sub_field( 'title' ); ?></h3>

					</div>
				</div>
				<?php $time = $time + 125; endwhile; endif; ?>
			</div>
		</div>
	</section>
<?php endwhile; endif; ?>

