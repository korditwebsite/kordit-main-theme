<section class="single-service">
	<div class="container">
		<div class="row">
			<div class="col-xl-3">
				<aside>
					<h2><?php _e( 'Navigation', 'textdomain' ); ?></h2>
					<ul>
						<?php
						$args = array(
							'post_type'   => "service",
							'post_status' => 'publish',
							'order' => 'ASC',
						);
						?>
						<?php $testimonials = new WP_Query( $args ); 
						if( $testimonials->have_posts() ) :
							?>
							<?php
							while( $testimonials->have_posts() ) :
								$testimonials->the_post();
								?>
								<li>
									<a href="<?php echo get_permalink($parent_id); ?>"><?php the_title(); ?></a>
								</li>
								<?php
							endwhile;
							wp_reset_postdata();
							?>
							<?php
						else :
							esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
						endif;
						?>
					</ul>
				</aside>
			</div>
			<div class="col-xl-9">
				<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'full-size' ); ?>
				<img class="img-fluid" src="<?php echo $url ?>" />
				<div class="inner-content mt-4">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if ( have_rows( 'custom_service' )) : while ( have_rows( 'custom_service' )  ) : the_row(); ?>
	<?php the_sub_field( 'subtitle' ); ?>
	<?php endwhile; endif; ?>