	<section class="single-service">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<?php $url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>
					<img class="img-fluid" src="<?php echo $url ?>" />
				</div>
				<div class="col-xl-12 mt-4">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</section>