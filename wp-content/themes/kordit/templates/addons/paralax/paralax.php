<?php 
$idsection = get_sub_field("id_sekcji");
$title = get_sub_field("tytul");
$subtitle = get_sub_field("opis");

$backgroundparalax = wp_get_attachment_image_url( get_sub_field('background_paralax'), "hero_image" );

if ( have_rows( 'options_paralax' ) ) :  while ( have_rows( 'options_paralax' ) ) : the_row(); 
	$paralax_bg_color = get_sub_field( 'background_color' );
	$paralax_opacity = get_sub_field( 'opacity' );
	$paralax_color_title = get_sub_field( 'color_title' );
	$paralax_color_subtitle = get_sub_field( 'color_subtitle' );

endwhile; endif; ?>

<section class="paralax-section" style="background-color: <?php echo $paralax_bg_color; ?>" id="<?php echo $idsection; ?>">
	<div class="paralax-background" style="background-image: url(<?php echo $backgroundparalax; ?>); opacity: <?php  echo $paralax_opacity; ?>"></div>
	<div class="inner-paralax">
		<h2 class="wow fadeInUp" style="color:<?php echo $paralax_color_title; ?>"><?php echo $title; ?></h2>
		<p class="wow fadeInDown" style="color: <?php echo $paralax_color_subtitle; ?>;"><?php echo $subtitle; ?></p>
	</div>
</section>