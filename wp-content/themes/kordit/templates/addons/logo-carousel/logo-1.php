<?php 

//Info for logotype
if( have_rows('logo') ): $x = 0; $time = 0; while ( have_rows('logo') ) : the_row();
	$idsection = get_sub_field("id_sekcji");
	$link = get_sub_field("link");
	$text = the_sub_field( 'title' );
endwhile; else : endif;

//Size container
if ( have_rows( 'size_container' ) ) : while ( have_rows( 'size_container' ) ) : the_row(); 
	$datamobile = get_sub_field( 'mobile' ); 
	$datatablet = get_sub_field( 'tablet' ); 
	$datadesktop = get_sub_field( 'desktop' ); 
	$dataall = get_sub_field( 'in_one_page' ); 
	$enddataall = $dataall -1;
endwhile; endif;

//output var
$mobile = "col-" . $datamobile . " ";
$tablet = "col-md-" . $datatablet . " ";
$desktop = "col-xl-" . $datadesktop . " ";

?>


<section class="section-logo-carousel" id="<?php echo $idsection; ?>">
	<div class="container">
		<div class="row">
			<h2><?php echo $text; ?></h2>
		</div>
		<div class="row">
			<div id="carousel-logo" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">
					<?php if( have_rows('logo') ): $x = 0; $time = 0; while ( have_rows('logo') ) : the_row();
						if ($x == 0) {
							$active = "active";
						} else {
							$active = " ";
						}
						?>
						<?php if (($x % $dataall) == 0 || $x == 0) : ?>

							<div class="carousel-item <?php echo $active; ?>">
								<div class="row">
								<?php endif ?>
								<div class="<?php echo $mobile . $tablet . $desktop; ?>">
									<a class="wow fadeInRight" data-wow-delay="<?php echo $time; ?>ms" href="<?php the_sub_field("link") ?>" target="_blank">
										<?php echo id_subimage("grafika", "small-logo"); ?>
									</a>
								</div>
								<?php if (($x % $dataall) == $enddataall) : ?>
								</div>
							</div>

						<?php endif ?>
						<?php $x = $x +1;
					endwhile; else : endif; ?>
				</div>
				<a class="carousel-control-prev" href="#carousel-logo" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" href="#carousel-logo" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
</section>