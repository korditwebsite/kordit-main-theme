<?php 

//Size container
if ( have_rows( 'size_container' ) ) : while ( have_rows( 'size_container' ) ) : the_row(); 
	$datamobile = get_sub_field( 'mobile' ); 
	$datatablet = get_sub_field( 'tablet' ); 
	$datadesktop = get_sub_field( 'desktop' ); 
endwhile; endif;

//var
$crop = get_sub_field( 'crop_thumbnail' );
$cropsizedata = get_sub_field( 'crop_to' );
$value = get_sub_field( 'value' );
$cropsize = "height:" . $cropsizedata . $value . ";";
$images = get_sub_field('galeria');
$size = 'gallery-home';
$size2 = 'hero_image';

//output var
$mobile = "col-" . $datamobile . " ";
$tablet = "col-md-" . $datatablet . " ";
$desktop = "col-xl-" . $datadesktop . " ";

if ($crop) {
	$cropclass = "crop-image";
} else {
	$cropclass = " ";
}
?>

<section class="gallery">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<h2><?php the_sub_field("tytul"); ?></h2>
				<p><?php the_sub_field("opis"); ?></p>
			</div>
			<?php 
			if( $images ): ?>
				<?php $time = 0; foreach( $images as $image ): ?>
				<figure class="<?php echo $mobile . $tablet . $desktop; ?>">
					<a class="wow fadeInUp js-smartPhoto" data-caption="<?php echo $image['description'] ?>" href="<?php echo wp_get_attachment_image_url( $image['ID'], $size2 ); ?>">
						<img style="<?php echo $cropsize; ?>" class="img-fluid item-gallery <?php echo $cropclass; ?>" alt="item-gallery" src="<?php echo wp_get_attachment_image_url( $image['ID'], $size ); ?>" class="img-fluid">
					</a>
				</figure>
				<?php $time = $time +250; endforeach; ?>
			<?php endif;  ?>
		</div>
	</div>
</section>