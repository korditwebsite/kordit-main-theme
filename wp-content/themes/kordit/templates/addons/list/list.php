<?php 
if ( have_rows( 'container_size' ) ) :  while ( have_rows( 'container_size' ) ) : the_row(); 
	// $divingmobile = "12"; 
	// $divingtablet = "12"; 
	// $divingdesktop = "12"; 
	$divingmobile = get_sub_field( 'mobile' ); 
	$divingtablet = get_sub_field( 'tablet' ); 
	$divingdesktop = get_sub_field( 'desktop' ); 
endwhile; endif;
$sizecontainerdiving =  "col-xl-" . $divingdesktop . " col-md-" . $divingtablet . " col-" . $divingmobile;
?>
<style type="text/css">
	
</style>
<section class="list-section">
	<div class="title-container">
		<h3 style="color: <?php echo $text_color; ?>"><?php the_sub_field("title"); ?></h3>
		<p style="color: <?php echo $text_color; ?>"><?php the_sub_field("subtitle"); ?></p>
	</div>
	<div class="row">
		<?php
		$number_list = 0;
		if ( have_rows( 'list' ) ) : while ( have_rows( 'list' ) ) : the_row();
			$classlist = "list-" . $number_list;
			?>
			<div class="wrap-list <?php echo $sizecontainerdiving; ?>">
				<div class="title-wrapper">
					<div class="title"><?php the_sub_field( 'name_item' ); ?></div>
					<div class="image"><?php echo id_subimage("image", "malyprostokat"); ?></div>
				</div>
				<ul class="<?php echo $classlist; ?>">
					<?php if ( have_rows( 'sublist' ) ) : while ( have_rows( 'sublist' ) ) : the_row(); ?>
						<li><?php the_sub_field( 'single_item' ); ?></li>
					<?php  endwhile; endif; ?>
				</ul>
			</div>
			<?php $number_list = $number_list + 1; endwhile; endif; ?>
		</div>
	</section>

