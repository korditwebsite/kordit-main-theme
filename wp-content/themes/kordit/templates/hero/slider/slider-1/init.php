<section class="theme-section-slider" id="<?php the_sub_field("id_naglowka"); ?>">
  <?php if( have_rows('slider-content') ): ?>
    <div id="slider-carousel" class="carousel slide carousel-fade" data-ride="carousel">

      <ol class="carousel-indicators">
        <?php $x = 0; while ( have_rows('slider-content') ) : the_row();
        if ($x <= 0) {
          $active ="active";
        } else {
          $active = " ";
        }
        ?>
        <li data-target="#slider" data-slide-to="<?php echo $x ?>" class="<?php echo $active ?>"></li>

        <?php $x = $x + 1; endwhile; ?>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!--First slide-->
        <?php $x = 0; while ( have_rows('slider-content') ) : the_row();
        if ($x <= 0) {
          $active ="active";
        } else {
          $active = " ";
        }
        ?>

        <div class="carousel-item <?php echo $active ?>">
          <div class="view">
            <?php $grafika = wp_get_attachment_image_url( get_sub_field('grafika'), "full" ); ?>
            <?php echo wp_get_attachment_image( get_sub_field('grafika'), "full", "", array( "class" => "theme-section-slider__image", "data-src=" => $grafika ) );  ?>
            <div class="mask rgba-black-light"></div>
          </div>

          <div class="theme-section-slider__carousel-text">
            <h3 class="wow fadeInDown theme-section-slider__carousel-text--title" data-wow-delay=".25s">
              <?php the_sub_field('naglowek'); ?>
            </h3>
            <p class="wow fadeInDown theme-section-slider__carousel-text--subtitle" data-wow-delay=".5s">
              <?php the_sub_field('podpis'); ?>
            </p>
            <?php if( have_rows('przycisk') ): while( have_rows('przycisk') ): the_row(); ?>
              <a data-wow-delay=".75s" class="wow fadeInDown theme-section-slider__carousel-text--link" href="<?php the_sub_field('link'); ?>">
                <?php the_sub_field('napis'); ?>
              </a>
            </div>
          <?php endwhile; endif; ?>
        </div>

        <?php $x = $x + 1; endwhile; ?>
        <!--/Third slide-->
      </div>
      <?php $licznikslajdow = 0; while ( have_rows('slider-content') ) : the_row();
      if ($licznikslajdow == 0) {
        $display ="d-none";
      } else {
        $display = " ";
      }
      ?>
      
      <a class="carousel-control-prev <?php echo $display; ?>" href="#slider-carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next <?php echo $display; ?>" href="#slider-carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
      <?php $licznikslajdow = $licznikslajdow + 1; endwhile; ?>
    </div>
    <?php  else : endif; ?>
  </section>