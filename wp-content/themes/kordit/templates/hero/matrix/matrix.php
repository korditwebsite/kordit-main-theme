<?php 
$background = wp_get_attachment_image_url( get_sub_field('background'), "full" );
$bgcolor = get_sub_field( 'background_color' );
$opacity = get_sub_field( 'opacity' );
$text = get_sub_field( 'main_text' );
$logo = wp_get_attachment_image( get_sub_field('logo'), "full" );
$vachromatic = get_sub_field( 'monochrome' );
$chromatic = "color-" . strtolower($vachromatic);
js_matrix();
?>
<style type="text/css">
	
</style>
<section class="matrix">
	<div class="container-matrix">
		<div class="content">
			<div class="background" style="background-image: url(<?php echo $background; ?>)"></div>
			<div class="content__title animation-slider">
				<div class="logotype-present wow zoomInUp" data-wow-delay="0.5s">
					<?php echo $logo; ?>
				</div>
				<p class="content__tagline wow zoomInUp" data-wow-delay="1s">Tworzymy nowoczesne i responsywne strony www</p>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var imageUrl = "<?php echo $background; ?>";
		jQuery(document).ready(function($){
			$('.background__copy').css('background-image', 'url("' + imageUrl + '")');
		});
	</script>
</body>
</section>