<section class="choose">
	<div class="container-row">
		<div class="wrapper-half">
			<div class="half">
				<div class="choose-inner-html">
					<?php if ( have_rows( 'left_image' ) ) : while ( have_rows( 'left_image' ) ) : the_row(); ?>
						<div class="choose-bg">
							<div class="content">
								<h3><?php the_sub_field( 'title' ); ?></h3>
								<h5><?php the_sub_field( 'subtitle' ); ?></h5>
								<?php if ( have_rows( 'button' ) ) : while ( have_rows( 'button' ) ) : the_row(); ?>
									<a href="<?php the_sub_field( 'link' ); ?>">
										<button><?php the_sub_field( 'text' ); ?></button>
									</a>
								<?php endwhile; endif; ?>
							</div> 
							<div class="bg-image">
								<?php echo wp_get_attachment_image( get_sub_field('image'), "full" ); ?>
							</div>

						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
			<div class="half">
				<div class="choose-inner-html">
					<?php if ( have_rows( 'right_image' ) ) : while ( have_rows( 'right_image' ) ) : the_row(); ?>
						<div class="choose-bg">
							<div class="content">
								<h3><?php the_sub_field( 'title' ); ?></h3>
								<h5><?php the_sub_field( 'subtitle' ); ?></h5>
								<?php if ( have_rows( 'button' ) ) : while ( have_rows( 'button' ) ) : the_row(); ?>
									<a href="<?php the_sub_field( 'link' ); ?>">
										<button><?php the_sub_field( 'text' ); ?></button>
									</a>
								<?php endwhile; endif; ?>
							</div>
							<div class="bg-image">
								<?php echo wp_get_attachment_image( get_sub_field('image'), "full" ); ?>
							</div>
						</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
</section>