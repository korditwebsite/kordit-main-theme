<?php if ( have_rows( 'ustawnienia_footera', 'option' ) ) : 
    while ( have_rows( 'ustawnienia_footera', 'option' ) ) : the_row();
        $linkfb = get_sub_field( 'link_do_facebooka' );
        $widgetfb = get_sub_field( 'wyswietl_widget_z_boku' ); 
        $wyborfootera = get_sub_field( 'wyglad_footera' ); 
    endwhile; endif; ?>
    <?php if( have_rows('ustawnienia_footera', 'option') ): while( have_rows('ustawnienia_footera', 'option') ): the_row(); ?>
        <div class="footer-bg">
            <div class="container footer-container">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-12">
                        <a class="navbar-brand" href="/">
                         <h5>O nas:</h5>
                     </a>
                     <?php the_sub_field('informacje', 'option'); ?>
                 </div>
                 <div class="col-xl-3 col-md-6 col-12 footer-box">
                    <h5>Kontakt z nami:</h5>
                    <ul class="footer-contact">
                        <li>
                            <img src="/wp-content/themes/kordit/assets/img/adress.svg" alt="icon"><p><?php the_sub_field('adress', 'option'); ?></p>
                        </li>
                        <?php if( have_rows('maile') ): while( have_rows('maile') ): the_row(); ?>
                            <li>
                                <img src="/wp-content/themes/kordit/assets/img/mail.svg" alt="icon">
                                <a href="mailto:<?php the_sub_field('mail', 'option'); ?>"><?php the_sub_field('mail', 'option'); ?></a>
                            </li>
                        <?php endwhile; endif; ?>
                        <?php if( have_rows('telefony') ): while( have_rows('telefony') ): the_row(); ?>
                            <li>
                                <img src="/wp-content/themes/kordit/assets/img/phone.svg" alt="icon">
                                <a href="tel:+48<?php the_sub_field('telefon', 'option'); ?>"><?php the_sub_field('telefon', 'option'); ?></a>
                            </li>
                        <?php endwhile; endif; ?>
                    </ul>
                </div>
                <div class="col-xl-3 col-md-6 col-12">
                    <div class="menu-footer-menu-container">
                        <h5>Menu:</h5>
                        <?php
                        wp_nav_menu([
                            'menu'            => 'top',
                            'theme_location'  => 'top',
                            'container'       => 'div',
                            'depth'           => 1,
                            'fallback_cb'     => 'bs4navwalker::fallback',
                            'walker'          => new bs4navwalker(),
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6 col-12">
                    <div class="menu-footer-menu-container">
                        <h5>Facebook:</h5>
                        <div class="fb-page" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"></div>
                        <div class="fb-page" data-href="<?php echo $linkfb; ?>" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="<?php echo $linkfb; ?>" class="fb-xfbml-parse-ignore"><a href="<?php echo $linkfb; ?>"></a></blockquote></div>
                        <script>(function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = 'https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v3.1&appId=2001495613512113&autoLogAppEvents=1';
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    </script>
                </div>
            </div>
        </div>
    </div>
    <!--Copyright-->
    <div class="footer-copyright">
        <p>Copyright: Kordit 2019</p>
        <a href="https://kordit.pl" aria-label="Strony internetowe Lublin" rel="noopener" target="_blank">
            <img class="lazy" alt="logo" src="https://kordit.pl/wp-content/uploads/2019/01/cropped-logob.png">
        </a>
    </div>
</div>
<?php endwhile; endif; ?>