<div class="map">
    <iframe src="https://snazzymaps.com/embed/133647" width="100%" height="400px" style="border:none;"></iframe>
</div>
<?php if( have_rows('ustawnienia_footera', 'option') ): while( have_rows('ustawnienia_footera', 'option') ): the_row(); ?>
    <div class="container">
        <div class="row footer-wrapper">
            <div class="col-xl-6 footer-contact-box">
                <h5>O firmie</h5>
                <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                    echo '<img alt="logotyp" class="img-fluid" src="'. esc_url( $logo[0] ) .'">';
                } else {
                    echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
                } ?>
                <p><?php the_sub_field('informacje', 'option'); ?></p>
            </div>
            <div class="col-xl-3 footer-contact-box2">
                <h5>Dane Kontaktowe</h5>
                <ul>
                    <li>
                        <i class="far fa-envelope"></i>
                        <a href="mailto:<?php the_sub_field('mail', 'option'); ?>"><?php the_sub_field('mail', 'option'); ?></a>
                    </li>
                    <li>
                        <i class="fas fa-phone"></i>
                        <a href="tel:<?php the_sub_field('telefon', 'option'); ?>"><?php the_sub_field('telefon', 'option'); ?></a>
                    </li>
                </ul>
            </div>
            <div class="col-xl-3">
                <h5 class="">Usługi</h5>
                <div class="menu-footer-menu-container">
                    <?php
                    wp_nav_menu([
                        'menu'            => 'footer',
                        'theme_location'  => 'footer',
                        'container'       => 'div',
                        'container_class' => 'nawigacja-menu',
                        'menu_class'      => 'navbar-nav',
                        'depth'           => 2,
                        'fallback_cb'     => 'bs4navwalker::fallback',
                        'walker'          => new bs4navwalker(),
                    ]);
                    ?>
                </div>
            </div>
        </div>
        <hr>
    </div>
    <!--Copyright-->
    <div class="footer-copyright py-3">
        <p>© 2019 Copyright:</p>
        <a href="http://www.factorywebsite.pl " target="_blank">
            <img src="/wp-content/themes/kordit/img/kordit.png">
        </a>
    </div>
    <!--/.Copyright-->
    <div class="scroll-up" id="scroll-up">
        <a href="#top"><img src="/wp-content/themes/kordit/img/up-chevron.png"></a>
    </div>
</div>
<?php endwhile; endif; ?>
