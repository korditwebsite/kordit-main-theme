<?php 
$colortext = get_sub_field( 'color_text' );
$colorbg = get_sub_field( 'background_color' );
$opacity = get_sub_field( 'opacity_background' );
$svgcolor = get_sub_field( 'color_text' );
$imagebg = id_subimage_url("tlo", "hero_image");
$versioncolor = "color-" . get_sub_field( 'versioncolor' ); 
?>
<style type="text/css">
	svg path {
		fill: <?php echo $colortext; ?> !important;
	}
	.color-color input, .color-color textarea {
		border: solid 1px <?php echo $colortext; ?> !important;
		color: <?php echo $colortext; ?>;
	}
</style>
<section class="kontakt kontakt-1 <?php echo $versioncolor; ?>" id="<?php the_sub_field("id_sekcji"); ?>" style="background-color: <?php echo $colorbg; ?>">
	<?php get_sub_field('tlo'); ?>
	<div class="bg-absolute" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('tlo'), "hero_image" ); ?>); opacity: <?php echo $opacity; ?>;">
	</div>
</div>
<div class="container">
	<?php 
	$mobile = "/wp-content/themes/kordit/img/mail.svg"; 
	?>
	<div class="row wowparalax">
		<div class="col-xl-6">
			<div class="inner-opis">
				<h2 style="color:<?php echo $colortext; ?>;"><?php the_sub_field("tytul"); ?></h2>
				<div class="phone wow">

					<?php if( have_rows('numery_telefonow') ): while ( have_rows('numery_telefonow') ) : the_row(); ?>
						<?php exportsvg("/assets/img/phone-2.svg"); ?>
						<a style="color:<?php echo $colortext; ?>;" href="<?php the_sub_field('numer_tel'); ?>"><?php the_sub_field('numer_tel'); ?></a>
					<?php endwhile; else : endif; ?>
				</div>
				<div class="mail wow">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="512px" height="512px">
						<g>
							<g>
								<g>
									<path d="M486.4,59.733H25.6c-14.138,0-25.6,11.461-25.6,25.6v341.333c0,14.138,11.461,25.6,25.6,25.6h460.8     c14.138,0,25.6-11.461,25.6-25.6V85.333C512,71.195,500.539,59.733,486.4,59.733z M494.933,426.667     c0,4.713-3.82,8.533-8.533,8.533H25.6c-4.713,0-8.533-3.82-8.533-8.533V85.333c0-4.713,3.82-8.533,8.533-8.533h460.8     c4.713,0,8.533,3.82,8.533,8.533V426.667z" fill="#FFFFFF"/>
									<path d="M470.076,93.898c-2.255-0.197-4.496,0.51-6.229,1.966L266.982,261.239c-6.349,5.337-15.616,5.337-21.965,0L48.154,95.863     c-2.335-1.96-5.539-2.526-8.404-1.484c-2.865,1.042-4.957,3.534-5.487,6.537s0.582,6.06,2.917,8.02l196.864,165.367     c12.688,10.683,31.224,10.683,43.913,0L474.82,108.937c1.734-1.455,2.818-3.539,3.015-5.794c0.197-2.255-0.51-4.496-1.966-6.229     C474.415,95.179,472.331,94.095,470.076,93.898z" fill="#FFFFFF"/>
									<path d="M164.124,273.13c-3.021-0.674-6.169,0.34-8.229,2.65l-119.467,128c-2.162,2.214-2.956,5.426-2.074,8.392     c0.882,2.967,3.301,5.223,6.321,5.897c3.021,0.674,6.169-0.34,8.229-2.65l119.467-128c2.162-2.214,2.956-5.426,2.074-8.392     C169.563,276.061,167.145,273.804,164.124,273.13z" fill="#FFFFFF"/>
									<path d="M356.105,275.78c-2.059-2.31-5.208-3.324-8.229-2.65c-3.021,0.674-5.439,2.931-6.321,5.897     c-0.882,2.967-0.088,6.178,2.074,8.392l119.467,128c3.24,3.318,8.536,3.442,11.927,0.278c3.391-3.164,3.635-8.456,0.549-11.918     L356.105,275.78z" fill="#FFFFFF"/>
								</g>
							</g>
						</g>
					</svg>

					<?php if( have_rows('adres_e-mail') ): while ( have_rows('adres_e-mail') ) : the_row(); ?>
						<a style="color:<?php echo $colortext; ?>;" href="<?php the_sub_field('mail'); ?>"><?php the_sub_field('mail'); ?></a>
					<?php endwhile; else : endif; ?>
				</div>
				<div class="home wow">
					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve" width="512px" height="512px">
						<g>
							<g>
								<path d="M506.555,208.064L263.859,30.367c-4.68-3.426-11.038-3.426-15.716,0L5.445,208.064 c-5.928,4.341-7.216,12.665-2.875,18.593s12.666,7.214,18.593,2.875L256,57.588l234.837,171.943c2.368,1.735,5.12,2.57,7.848,2.57    c4.096,0,8.138-1.885,10.744-5.445C513.771,220.729,512.483,212.405,506.555,208.064z" fill="#FFFFFF"/>
							</g>
						</g>
						<g>
							<g>
								<path d="M442.246,232.543c-7.346,0-13.303,5.956-13.303,13.303v211.749H322.521V342.009c0-36.68-29.842-66.52-66.52-66.52    s-66.52,29.842-66.52,66.52v115.587H83.058V245.847c0-7.347-5.957-13.303-13.303-13.303s-13.303,5.956-13.303,13.303v225.053    c0,7.347,5.957,13.303,13.303,13.303h133.029c6.996,0,12.721-5.405,13.251-12.267c0.032-0.311,0.052-0.651,0.052-1.036v-128.89    c0-22.009,17.905-39.914,39.914-39.914s39.914,17.906,39.914,39.914v128.89c0,0.383,0.02,0.717,0.052,1.024    c0.524,6.867,6.251,12.279,13.251,12.279h133.029c7.347,0,13.303-5.956,13.303-13.303V245.847    C455.549,238.499,449.593,232.543,442.246,232.543z" fill="#FFFFFF"/>
							</g>
						</g>
					</svg>
					<div class="text" style="color:<?php echo $colortext; ?>;">
						<?php the_sub_field('adres'); ?>
					</div>
				</div>
				<div class="text mt-4 w-100" style="color:<?php echo $colortext; ?>;">
					<?php the_sub_field('informacje_dodatkowe'); ?>
				</div>
			</div>
		</div>
		<div class="col-xl-6">
			<?php echo do_shortcode('[contact-form-7 id="5" title="Formularz 1"]'); ?>
		</div>
	</div>
</div>
</section>
<div class="container">
	<div class="col-xl-12 my-5">
		<?php if (get_sub_field( 'mapa_google' )): ?>
			<iframe src="<?php the_sub_field( 'mapa_google' ); ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		<?php endif; ?>
	</div>
</div>